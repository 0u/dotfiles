set et sw=4 ts=4

nn <leader>r :!python3 -u %<cr>
nn <leader>t :!python3 -m unittest<cr>
