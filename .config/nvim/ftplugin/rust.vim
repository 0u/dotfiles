set equalprg=rustfmt

func! Cargo(...)
  exe ":wa"
  exe "15sp term://cargo " . expand(join(a:000))
endf

let g:rustfmt_autosave = 1

let g:racer_cmd = "/home/peep/.cargo/bin/racer"

nn <leader>r :call Cargo("run")<cr>
nn <leader>b :call Cargo("build")<cr>
nn <leader>c :call Cargo("check")<cr>
nn <leader>t :call Cargo("test")<cr>
nn <leader>d :call Cargo("doc --open")<cr>

nmap <leader>gd <Plug>(rust-def)
nmap <leader>gh <Plug>(rust-doc)

ALEDisable
