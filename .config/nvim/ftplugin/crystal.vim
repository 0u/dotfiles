" Run `crystal tool format` on save
"let g:crystal_auto_format = 1

" tab settings
set ts=2 sw=2 et

func! Crystal( ... )
    exe "25sp term://crystal " . shellescape(expand(join(a:000)))
    norm! A
endf

" General
nn <leader>b :25sp term://crystal build %<cr>
nn <leader>r :25sp term://crystal run %<cr>
nn <leader>t :25sp term://crystal spec<cr>

" Navigation
nn <leader>gh :CrystalHierarchy<space>
nn <leader>gd :CrystalDef<cr>

" Testing
nn <leader>gt :CrystalSpecRunAll<cr>
