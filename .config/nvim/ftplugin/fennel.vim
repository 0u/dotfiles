func! CompileFennel()
  let l:file = expand("%")
  let l:out  = l:file[:strlen(l:file)-5]

  exe "!sh -c 'fennel --compile " . l:file . " > " . l:out . ".lua'"
endf

nn <leader>b :w<cr>:call CompileFennel()<cr>
nn <leader>r :w<cr>:!fennel %<cr>
