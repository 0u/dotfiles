set ts=2 sw=2

" Automatically add 'end' when i open a 'do' block.
ino do<cr> do<cr>endO

nn <leader>t :!mix test<cr>
nn <leader>r :!elixir %<cr>

" Causes bugs
" let g:mix_format_on_save = 1

" otp and elixir directories must be in there
" for jumping to erlang/elixir definitions.
let g:alchemist#elixir_erlang_src = "~/elixir/src/"
