" Nicer highlighting
hi! link moonFunction Type
hi! link moonLuaFunc  PreProc
hi! link moonSpecialOp text

" RunWith runs the current file with a program and args
func! RunWith(...)
	:exe "!" . shellescape(expand(join(a:000))) . " " . shellescape(expand("%"))
endf

nn <leader>r :call RunWith("moon")<cr>
nn <leader>b :call RunWith("moonc")<cr>
