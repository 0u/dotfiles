set ts=2 shiftwidth=2

let g:paredit_electric_return=0
let g:paredit_mode=0
let g:lisp_rainbow=1

" ToggleWhitespace

func! Sbcl( ... )
  let l:file = expand("%")

  exe "20sp term://sbcl " . shellescape(expand(join(a:000))) . " " . shellescape(l:file)
endf

nn <leader>i :call Sbcl("-i")<cr>
nn <leader>r :call Sbcl("--script")<cr>
