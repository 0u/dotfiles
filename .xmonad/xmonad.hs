import           XMonad
import           XMonad.Layout.Spacing
import           System.Posix          (executeFile)
import           Control.Monad         (void)
import qualified Data.Map              as M

---------------------------------------------------------------------
--                   Configuration constants.                      --
---------------------------------------------------------------------
myTerminal       = "alacritty"
myNetworkManager = "st -e nmtui"
xmonadConfigPath = "~/.xmonad/xmonad.hs"

---------------------------------------------------------------------
--                     Helper functions.                           --
---------------------------------------------------------------------

(?) :: Monad m => m Bool -> m a -> m a -> m a
(?) cond f1 f2 = cond >>= pick
    where pick True  = f1
          pick False = f2
infixr 1 ?

-- a variation of spawn that does not envoke a shell.
spawnCmd :: MonadIO m => String -> [String] -> m ()
spawnCmd program args = void $ xfork $ executeFile program True args Nothing

notifySend :: [String] -> X ()
notifySend = spawnCmd "notify-send"

-- recompile and restart xmonad
recompileRestart :: X ()
recompileRestart = void $ do
    notifySend ["Restarting XMonad...", "-u", "low"]
    recompile True
        ? restart "xmonad" True
        $ notifySend ["Failed to recompile XMonad!", "-u", "critical", "-t", "9000"]

-- run a shell command in my terminal
inTerminal :: MonadIO m => String -> m ()
inTerminal args = spawnCmd myTerminal ["-e", "sh", "-c", args]

---------------------------------------------------------------------
--     Key bindings, Add, modify or remove key bindings here.      --
---------------------------------------------------------------------

myKeys :: XConfig Layout -> M.Map (ButtonMask, KeySym) (X ())
myKeys conf@XConfig {modMask = mod} = M.fromList
    [ ((mod,      xK_q)         , kill)                   -- kill current window
    , ((mod,      xK_r)         , recompileRestart)       -- restart xmonad
    , ((mod,      xK_d)         , spawn "dmenu_run")      -- run a program
    , ((mod,      xK_i)         , spawn myNetworkManager) -- open network gui

    -- Managed by sxhkd, will not be ran unless something happens to sxhkd,
    -- I am leaving them here as backups though.
    , ((mod,      xK_Return)    , spawn $ terminal conf)  -- spawn a terminal
    , ((mod,      xK_x)         , spawn "lockscreen")     -- lock the screen
    , ((mod,      xK_b)         , spawn "books")          -- open a book
    ]
        where modShift = mod .|. shiftMask


---------------------------------------------------------------------
--                     Layout configuration                        --
---------------------------------------------------------------------
myLayout = tiled ||| Mirror tiled ||| Full
    where
        -- FIXME: smartSpacing is deprecated
        tiled   = smartSpacing 5 $ Tall nmaster delta ratio
        nmaster = 1
        ratio   = 1/2
        delta   = 3/100

---------------------------------------------------------------------
--                     XMonad main entry point                     --
---------------------------------------------------------------------

main :: IO ()
main = xmonad def
    { terminal     = myTerminal
    , modMask      = mod4Mask
    -- TOOD: Figure out how to only show the boarder when
    -- i have more then one window open.
    , borderWidth  = 0
    , keys         = \c -> myKeys c `M.union` keys def c
    , layoutHook   = myLayout
    , startupHook  = notifySend ["Welcome to XMonad!", "-u", "low"]
    }
