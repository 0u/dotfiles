" tab settings
set ts=2 sw=2 et
hi! link rubyDefine Define

nn <leader>r :sp term://ruby %<cr>
