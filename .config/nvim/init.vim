"        _
" __   _(_)_ __ ___  _ __ ___
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__
"   \_/ |_|_| |_| |_|_|  \___|

call plug#begin('~/.config/nvim/plugged')

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                         General plugins                           "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'tpope/vim-surround'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'kien/rainbow_parentheses.vim'
Plug 'w0rp/ale'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'mhinz/vim-startify'

" Plug 'aurieh/discord.nvim'

" Snippets
Plug 'Shougo/neosnippet.vim'
Plug 'UlisseMini/neosnippet-snippets'
" Make neosnippet use tabs for expanding snippets
imap <expr><TAB>
      \ neosnippet#expandable_or_jumpable() ?
      \    "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
      \ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Golang development                          "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'fatih/vim-go',      { 'for': 'go' }
Plug 'sebdah/vim-delve',  { 'for': 'go' }
Plug 'buoto/gotests-vim', { 'for': 'go' }
Plug 'zchee/deoplete-go', { 'for': 'go', 'do': 'make' }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Rust development                            "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'rust-lang/rust.vim',   { 'for': 'rust' }
Plug 'racer-rust/vim-racer', { 'for': 'rust' }

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Elixir development                          "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'mhinz/vim-mix-format',      { 'for': 'elixir'}
Plug 'elixir-editors/vim-elixir', { 'for': 'elixir'}
Plug 'slashmili/alchemist.vim',   { 'for': 'elixir'}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Other languages                             "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'rhysd/vim-crystal',    { 'for': 'crystal' }
Plug 'leafo/moonscript-vim', { 'for': 'moon' }
" Plug 'ElmCast/elm-vim',      { 'for': 'elm' }
Plug 'ziglang/zig.vim'
Plug 'jakwings/vim-pony'
Plug 'wlangstroth/vim-racket'
Plug 'bakpakin/fennel.vim'
Plug 'Glench/Vim-Jinja2-Syntax', { 'for': 'html' }
Plug 'ollykel/v-vim'
Plug 'dart-lang/dart-vim-plugin'

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                      Better markdown support                      "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'plasticboy/vim-markdown'
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_fenced_languages = ['go=go', 'hs=haskell', 'lua=lua', 'py=python', 'python=python', 'viml=vim', 'bash=sh']

" Tiny plugins
Plug 'ntpeters/vim-better-whitespace'
Plug 'christoomey/vim-tmux-navigator'
Plug 'PotatoesMaster/i3-vim-syntax', { 'for': 'i3' }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                          ColorSchemes                             "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Plug 'romainl/flattened'
Plug 'morhetz/gruvbox'
Plug 'flazz/vim-colorschemes'
Plug 'felixhummel/setcolors.vim'
call plug#end()

" LanguageClient {{{
let g:LanguageClient_settingsPath=expand("~/.config/nvim/languageClient.json")
" let g:LanguageClient_serverCommands = {
"       \ 'rust': ['rustup', 'run', 'stable', 'rls'],
"       \ }

" Was not working for some reason
" 'dart': ['dart', '/opt/dart-sdk/bin/snapshots/analysis_server.dart.snapshot', '--lsp'],

command! Fmt call LanguageClient#textDocument_formatting()
"}}}

" Make deoplete load on startup
let g:deoplete#enable_at_startup = 1
set pumheight=10 " Completion window max size

" change the deoplete delay before autocomplete
let g:deoplete#auto_complete_delay = 0

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                         Basic vim settings                        "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
filetype plugin indent on
syntax enable
set encoding=utf-8
set ruler showmode noshowcmd
set autoread                 " read changes outside of vim automatically
set noerrorbells             " begone beeps
set autowrite                " Automatically save before :next, :make etc.
set hidden                   " Non retarded buffers
set history=1000             " vim ex mode history
set fileformats=unix,dos,mac " Prefer Unix over Windows over OS 9 formats
set copyindent               " copy existing indentation
set nohlsearch               " Don't highlight all search matches.
set linebreak
set number relativenumber

" Security
set nomodelineexpr nomodeline
set modelines=0

" colors
if has("termguicolors")
  set termguicolors
endif

" i don't like fill characters!
" TODO: Fix this so it *actaully sets the fill characters to space*
" vim seems to hate setting them to space, for some reason.
set fillchars="fold: ,stl: ,stlnc: "

" Undo settings
set undofile undodir=~/.config/nvim/undo
set undolevels=1000

" disable autocompletion preview
set completeopt-=preview

set numberwidth=1 " Use the least amount of space possible

" Set my leader key to space
nnoremap <Space> <nop>
let mapleader = " "

" default tab settings, overwritten for meny languages.
set tabstop=2
set shiftwidth=2

" use spaecs instead of tabs
set expandtab

" disable status bar
set laststatus=0

" Better pattern matching
set ignorecase
set smartcase

" folding
set foldmethod=manual

" Splits open at the bottom and right
set splitbelow splitright

" Better ex mode completion
set wildmenu

" Tweaks for file browsing
let g:netrw_banner=0     " disable anoying banner
let g:netrw_liststyle=3  " tree view
let g:netrw_winsize = 25 " window size
"}}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"            Leader bindings, more in ./ftplugin/*.vim              "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nn <leader>s :%s//g<left><left>
nn <leader>g :%g/
nn <leader>x :x<cr>
nn <leader>q :wqa<cr>
nn <leader>w :wa<cr>

nn <leader>f :FZF<cr>
nn <leader>b :call fzf#run({'source': map(filter(range(1, bufnr('$')), 'buflisted(v:val)'),
            \               'bufname(v:val)'),
            \ 'sink': 'e', 'down': '30%'})<cr><cr>


nn <leader>= mzgg=G`z
nn <leader>e :e<space>

" buffer navigation
nn <leader>n :bn<cr>
nn <leader>p :bp<cr>

" add semicolon at end of line
nn <leader>; A;<esc>

func MkdirThenSave()
  exe "!mkdir -p %:h"
  exe "w"
endfunction

nn <leader>W :call MkdirThenSave()<cr>

" Commands
command Hi :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
      \ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
      \ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<cr>

command Nm set number! relativenumber!
command Ctags !ctags -R .
command Rc FZF ~/.config/nvim
command W w

" useful for view the full output of :highlight
func TabMessage(cmd)
  redir => message
  silent execute a:cmd
  redir END
  if empty(message)
    echoerr "no output"
  else
    tabnew
    setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted nomodified
    silent put=message
  endif
endf

au Filetype asm set syntax=nasm
au BufRead,BufNewFile *.rkt set filetype=racket
au BufRead,BufNewFile *.cl  set filetype=lisp
au BufRead,BufNewFile *.fs  set filetype=forth

au VimEnter * silent! RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Disable line numbers in the terminal
au TermOpen * setlocal nonumber norelativenumber noruler noshowmode
" showmode is a global option, so on terminal close we need to reset it
au TermClose * setlocal showmode

" Disable auto commenting
" au FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Hide QFix buffer (quickfix)
augroup QFix
  autocmd!
  autocmd FileType qf setlocal nobuflisted
augroup END
"}}}

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Remappings                              "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Shortcutting split navigation, saving a keypress.
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Make pane navigation work from inside the terminal emulator.
tnoremap <C-w><C-h> <C-\><C-n><C-w>h
tnoremap <C-w><C-j> <C-\><C-n><C-w>j
tnoremap <C-w><C-k> <C-\><C-n><C-w>k
tnoremap <C-w><C-l> <C-\><C-n><C-w>l

" Exit the terminal with control e
tnoremap <C-e> <C-\><C-n>

" Center the screen when searching
nn n nzz

" make Y non retarded
nn Y y$

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                     Colorscheme & Highlighting                    "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
func ColorScheme()
  " use my terminal background colors not the colorschemes
  hi! Normal       ctermbg=NONE guibg=NONE
  hi! LineNr       ctermbg=NONE guibg=NONE
  hi! TabLine      ctermbg=NONE guibg=NONE
  hi! TabLineFill  ctermbg=NONE guibg=NONE
  hi! VertSplit    ctermbg=NONE guibg=NONE
  hi! StatusLine   cterm=NONE   gui=NONE
  hi! Folded       ctermbg=NONE guibg=NONE

  " Link some things
  hi! link StatusLineNC StatusLine
  hi! link vimfunction Function
endf
au ColorScheme * call ColorScheme()

set background=dark
let g:gruvbox_italicize_comments = 0
let g:gruvbox_italic             = 1

silent! colo gruvbox
hi! Operator gui=bold cterm=bold

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                Copy and paste to the system clipboard             "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
vnoremap <C-c> "+y

let g:clipboard = {
      \   'name': 'xclip',
      \   'copy': {
      \      '+': 'xclip -i -selection clipboard',
      \      '*': 'xclip -i -selection clipboard',
      \    },
      \   'paste': {
      \      '+': 'xclip -o -selection clipboard',
      \      '*': 'xclip -o -selection clipboard',
      \   },
      \   'cache_enabled': 1,
      \ }
"}}}
